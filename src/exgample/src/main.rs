#[macro_use(gen_expr, gen_bindings)]
extern crate liblanq;

use liblanq::{Expression, TypeSig, Value};

#[derive(Debug, Clone)]
enum RInt {
    Incr(Box<RInt>),
    Zero,
}

impl RInt {
    fn lanq_string(&self) -> String {
        match self {
            RInt::Incr(inner) => format!("RInt::Incr({})", inner.lanq_string()),
            RInt::Zero => "RInt::Zero".to_string(),
        }
    }
}

#[derive(Debug)]
struct Character {
    name: String,
    health: RInt,
    strength: RInt,
    agility: RInt,
    intelligence: RInt,
    single_weapon_attack_bonus_lambda: Expression,
    double_weapon_attack_bonus_lambda: Expression,
}

impl Character {
    fn new(
        name: String,
        health: RInt,
        strength: RInt,
        agility: RInt,
        intelligence: RInt,
    ) -> Character {
        let bindings = Character::get_lanq_bindings();
        Character {
            name,
            health,
            strength,
            agility,
            intelligence,
            single_weapon_attack_bonus_lambda: gen_expr!(
                "(lambda |character: Character| -> RInt character.agility)".to_string(),
                &TypeSig::Function(
                    vec![TypeSig::Reference("Character".to_string())],
                    Box::new(TypeSig::Reference("RInt".to_string()))
                ),
                &bindings,
                &Vec::new()
            ),
            double_weapon_attack_bonus_lambda: gen_expr!(
                "(lambda |character: Character| -> RInt character.strength)".to_string(),
                &TypeSig::Function(
                    vec![TypeSig::Reference("Character".to_string())],
                    Box::new(TypeSig::Reference("RInt".to_string()))
                ),
                &bindings,
                &Vec::new()
            ),
        }
    }

    fn get_lanq_bindings() -> liblanq::SigAnnotatedBindings {
        let bindings = gen_bindings!("
        type Bool enum {
            True,
            False,
        }

        let true: Bool = Bool::True
        let false: Bool = Bool::False

        type RInt enum {
            Incr(RInt),
            Zero,
        }

        lazy_fn add(lhs: RInt, rhs: RInt) -> RInt
            (match lhs
                RInt::Incr(sub_lhs) => (add sub_lhs RInt::Incr(rhs))
                RInt::Zero => rhs
            )

        type Character struct {
            health: RInt,
            strength: RInt,
            agility: RInt,
            intelligence: RInt,
        }"
        .to_string());
        return bindings;
    }

    fn get_single_ab(&self) -> Result<i32, Error> {
        self.evaluate_expr_for_int(self.single_weapon_attack_bonus_lambda.clone())
    }

    fn get_double_ab(&self) -> Result<i32, Error> {
        self.evaluate_expr_for_int(self.double_weapon_attack_bonus_lambda.clone())
    }

    fn evaluate_expr_for_int(&self, lambda: Expression) -> Result<i32, Error> {
        let bindings = Character::get_lanq_bindings();
        let character_providing_function_call = Expression::FnCall(
            Box::new(lambda),
            vec![self.get_character_expr().map_err(Error::Lanq)?],
        );
        match liblanq::eval_expr(
            character_providing_function_call,
            &mut bindings.into_type_value_bindings(),
        ) {
            Ok(v) => rint_value_from_value(v),
            Err(e) => Err(Error::Lanq(e)),
        }
    }

    fn get_character_expr(&self) -> Result<liblanq::Expression, liblanq::Error> {
        let bindings = Character::get_lanq_bindings();
        let constructor = format!(
            "Character{{ health: {}, strength: {}, agility: {}, intelligence: {}}}",
            self.health.lanq_string(),
            self.strength.lanq_string(),
            self.agility.lanq_string(),
            self.intelligence.lanq_string()
        );
        Ok(gen_expr!(
            constructor,
            &TypeSig::Reference("Character".to_string(),),
            &bindings,
            &Vec::new()
        ))
    }
}

fn rint_value_from_value(v: Value) -> Result<i32, Error> {
    match v.clone() {
        Value::Enum(enum_name, variant_name, values) => {
            if enum_name == "RInt" {
                match variant_name.as_ref() {
                    "Zero" => Ok(0),
                    "Incr" => {
                        if values.len() == 1 {
                            Ok(1 + rint_value_from_value(values.get(0).unwrap().clone())?)
                        } else {
                            Err(Error::UnexpectedReturn(v))
                        }
                    }
                    _ => Err(Error::UnexpectedReturn(v)),
                }
            } else {
                Err(Error::UnexpectedReturn(v))
            }
        }
        _ => Err(Error::UnexpectedReturn(v)),
    }
}

#[derive(Debug)]
enum Error {
    Lanq(liblanq::Error),
    UnexpectedReturn(Value),
}

fn main() {
    let zero: RInt = RInt::Zero;
    let one: RInt = RInt::Incr(Box::new(zero.clone()));
    let two: RInt = RInt::Incr(Box::new(one.clone()));
    let three: RInt = RInt::Incr(Box::new(two.clone()));
    let four: RInt = RInt::Incr(Box::new(three.clone()));
    let five: RInt = RInt::Incr(Box::new(four.clone()));

    let idrigoth = Character::new("Idrigoth".to_string(), five, two, four, three);
    println!("character: {:?}", idrigoth);
    println!("single: {:?}", idrigoth.get_single_ab());
    println!("single: {:?}", idrigoth.get_double_ab());
}
