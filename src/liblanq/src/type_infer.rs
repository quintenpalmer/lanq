use lookups;
use models::{Bindings, Error, Expression, SigAnnotatedBindings, Type, TypeSig};

pub fn infer_type_signature(
    expr: &Expression,
    bindings: &SigAnnotatedBindings,
    local_bindings: &Bindings<TypeSig>,
) -> Result<TypeSig, Error> {
    match expr {
        &Expression::Enum(ref name, _, _) => {
            let type_ = bindings
                .types
                .get(&name)
                .ok_or(Error::TypeDoesNotExist(name.clone()))?;
            match type_.type_ {
                Type::Enum(_) => Ok(TypeSig::Reference(name.clone())),
                _ => Err(Error::VariantRequestOfNonEnum(name.clone())),
            }
        }
        &Expression::Struct(ref name, _, _) => Ok(TypeSig::Reference(name.clone())),
        &Expression::Lambda(ref params, _, _, ref ret_type_sig, _) => {
            let mut param_type_sigs = Vec::new();
            for &(_, ref param_type_sig) in params.iter() {
                param_type_sigs.push(param_type_sig.clone());
            }
            Ok(TypeSig::Function(
                param_type_sigs,
                Box::new(ret_type_sig.clone()),
            ))
        }
        &Expression::VariableReference(ref name, ref maybe_ts) => match maybe_ts {
            &Some(ref ts) => Ok(ts.clone()),
            &None => match bindings.values_with_ts.get(&name) {
                Some((ref_expr, _)) => infer_type_signature(&ref_expr, bindings, local_bindings),
                None => local_bindings
                    .get(&name)
                    .ok_or(Error::VariableDoesNotExist(name.clone())),
            },
        },
        &Expression::FnCall(ref body, _) => {
            match infer_type_signature(body, bindings, local_bindings)? {
                TypeSig::Function(_, ref ret_type_sig) => Ok(*ret_type_sig.clone()),
                _ => Err(Error::TypeError),
            }
        }
        &Expression::StructFieldAccess(ref struct_, ref field_name) => {
            let struct_type_sig = infer_type_signature(struct_, bindings, local_bindings)?;
            let fields = match struct_type_sig {
                TypeSig::Reference(ref struct_name) => {
                    let field_type_sigs =
                        lookups::lookup_struct_field_types(struct_name, &bindings.types)?;
                    Ok(field_type_sigs)
                }
                _ => Err(Error::FieldAccessOfNonStruct),
            }?;
            match fields.get(field_name) {
                Some(v) => Ok(v.clone()),
                None => Err(Error::FieldDoesNotExist(field_name.clone())),
            }
        }
        &Expression::MatchExpr(ref cond_expr, ref branches) => {
            let branch_type_sig = infer_type_signature(cond_expr, bindings, local_bindings)?;
            if branches.len() == 0 {
                Err(Error::MatchExpressionWithNoBranches)
            } else {
                let &(ref branch_expr, ref then_expr) = branches.get(0).unwrap();
                let sub_local_bindings =
                    match_expression_new_bindings(branch_expr, &branch_type_sig, bindings)?;

                let mut new_local_bindings = local_bindings.clone();
                for (k, v) in sub_local_bindings.into_iter() {
                    new_local_bindings.bind(k, v)?;
                }
                infer_type_signature(then_expr, bindings, &new_local_bindings)
            }
        }
    }
}

pub fn match_expression_new_bindings(
    branch_expr: &Expression,
    branch_type_sig: &TypeSig,
    bindings: &SigAnnotatedBindings,
) -> Result<Bindings<TypeSig>, Error> {
    match *branch_expr {
        Expression::VariableReference(ref var_name, _) => {
            let mut new_local_bindings = Bindings::new();
            new_local_bindings.bind(var_name.clone(), branch_type_sig.clone())?;
            Ok(new_local_bindings)
        }
        Expression::Enum(ref enum_name, ref enum_variant_name, ref enum_variant_exprs) => {
            let variant_type_sigs =
                lookups::lookup_enum_variant_types(enum_name, enum_variant_name, &bindings.types)?;
            let mut new_local_bindings = Bindings::new();
            for (ref variant_expr, ref type_sig) in
                enum_variant_exprs.iter().zip(variant_type_sigs.iter())
            {
                for (name, ts) in
                    match_expression_new_bindings(variant_expr, type_sig, bindings)?.into_iter()
                {
                    new_local_bindings.bind(name, ts)?;
                }
            }
            Ok(new_local_bindings)
        }
        Expression::Struct(ref struct_name, ref struct_fields, ref struct_field_values) => {
            let field_type_sigs = lookups::lookup_struct_field_types(struct_name, &bindings.types)?;
            let mut new_local_bindings = Bindings::new();
            for field_name in struct_fields.iter() {
                let field_value = struct_field_values
                    .get(field_name)
                    .ok_or(Error::VariableDoesNotExist(field_name.clone()))?;
                let field_type_sig = field_type_sigs
                    .get(field_name)
                    .ok_or(Error::VariableDoesNotExist(field_name.clone()))?;
                for (name, ts) in
                    match_expression_new_bindings(field_value, field_type_sig, bindings)?
                        .into_iter()
                {
                    new_local_bindings.bind(name, ts)?;
                }
            }
            Ok(new_local_bindings)
        }
        _ => Ok(Bindings::new()),
    }
}
