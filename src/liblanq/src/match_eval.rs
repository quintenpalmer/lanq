use models::{Bindings, Error, Expression, Value};

pub fn resolve_match_expression(
    branch_expr: &Expression,
    cond_expr: &Value,
) -> Result<(bool, Bindings<Expression>), Error> {
    match branch_expr {
        Expression::VariableReference(ref name, _) => {
            let mut new_local_bindings = Bindings::new();
            new_local_bindings.bind(name.clone(), cond_expr.clone().into_expr())?;
            Ok((true, new_local_bindings))
        }
        Expression::Enum(ref enum_name, ref enum_variant_name, ref enum_variant_exprs) => {
            match cond_expr {
                Value::Enum(
                    ref cond_enum_name,
                    ref cond_enum_variant_name,
                    ref cond_enum_variant_exprs,
                ) => {
                    if enum_name != cond_enum_name {
                        return Err(Error::MatchArmMismatch(
                            enum_name.clone(),
                            cond_enum_name.clone(),
                        ));
                    }
                    if enum_variant_name != cond_enum_variant_name {
                        return Ok((false, Bindings::new()));
                    }
                    if enum_variant_exprs.len() != cond_enum_variant_exprs.len() {
                        return Err(Error::FunctionArgumentCountMismatch(
                            enum_variant_exprs.len() as i32,
                            cond_enum_variant_exprs.len() as i32,
                        ));
                    }

                    let mut new_local_bindings = Bindings::new();
                    for (ref variant_expr, ref cond_variant_expr) in enum_variant_exprs
                        .iter()
                        .zip(cond_enum_variant_exprs.iter())
                    {
                        let (matches, sub_bindings) =
                            resolve_match_expression(variant_expr, cond_variant_expr)?;
                        if !matches {
                            return Ok((false, Bindings::new()));
                        }
                        for (name, ts) in sub_bindings.into_iter() {
                            new_local_bindings.bind(name, ts)?;
                        }
                    }
                    Ok((true, new_local_bindings))
                }
                _ => Err(Error::TypeError),
            }
        }
        Expression::Struct(ref struct_name, ref fields, ref field_values) => match cond_expr {
            Value::Struct(ref cond_struct_name, ref cond_fields, ref cond_field_values) => {
                if struct_name != cond_struct_name {
                    return Err(Error::MatchArmMismatch(
                        struct_name.clone(),
                        cond_struct_name.clone(),
                    ));
                }

                if fields != cond_fields {
                    return Err(Error::StructFieldTypeMatchFieldMismatch(
                        struct_name.clone(),
                        cond_struct_name.clone(),
                        fields.clone(),
                        cond_fields.clone(),
                    ));
                }

                let mut new_local_bindings = Bindings::new();
                for field_name in fields.iter() {
                    let field_value = field_values
                        .get(field_name)
                        .ok_or(Error::VariableDoesNotExist(field_name.clone()))?;
                    let cond_field_value = cond_field_values
                        .get(field_name)
                        .ok_or(Error::VariableDoesNotExist(field_name.clone()))?;
                    let (matches, sub_bindings) =
                        resolve_match_expression(field_value, cond_field_value)?;

                    if !matches {
                        return Ok((false, Bindings::new()));
                    }

                    for (name, ts) in sub_bindings.into_iter() {
                        new_local_bindings.bind(name, ts)?;
                    }
                }

                Ok((true, new_local_bindings))
            }
            _ => Err(Error::TypeError),
        },
        _ => Err(Error::TypeError),
    }
}
