use models::{Error, Expression, SigAnnotatedBindings, Type, TypeSig, TypeWithGenerics, Value};

use parse;
use tokenize;

pub fn value_string(v: Value) -> String {
    value_string_helper(&v, 0)
}

pub fn expr_string(e: Expression) -> String {
    expr_string_helper(&e, 0)
}

pub fn write_pretty_file(source_contents: String) -> Result<String, Error> {
    let tokens = tokenize::tokenize(source_contents);
    let bindings = parse::parse_all_bindings(tokens)?;
    Ok(write_pretty_sig_annotated_bindings(&bindings))
}

pub fn write_pretty_sig_annotated_bindings(bindings: &SigAnnotatedBindings) -> String {
    let mut ret_string = String::new();
    for name in bindings.all_seen.iter() {
        match bindings.types.get(name) {
            Some(ref type_) => {
                let generic_string = if type_.generics.len() == 0 {
                    "".to_string()
                } else {
                    format!("<{}>", type_.generics.join(", "))
                };
                let (s_or_e, insides) = type_string(type_);
                ret_string = format!(
                    "{}type{} {} {} {{{}}}\n\n",
                    ret_string, generic_string, name, s_or_e, insides
                );
            }
            None => (),
        };
        match bindings.values_with_ts.get(name) {
            Some((ref expr, ref ts)) => match expr {
                Expression::Lambda(params_with_ts, body, _generics, ret_ts, lazy) => {
                    let arg_strings: Vec<String> = params_with_ts
                        .iter()
                        .map(|(name, ts)| format!("{}: {}", name, ts_string(ts)))
                        .collect();
                    ret_string = format!(
                        "{}{}fn {}({}) -> {}\n{}\n\n",
                        ret_string,
                        if *lazy { "lazy_" } else { "" },
                        name,
                        arg_strings.join(", "),
                        ts_string(ret_ts),
                        expr_string_helper(body, 1),
                    );
                }
                _ => {
                    ret_string = format!(
                        "{}let {}: {} =\n{}\n\n",
                        ret_string,
                        name,
                        ts_string(ts),
                        expr_string_helper(expr, 1)
                    );
                }
            },
            None => (),
        }
    }

    ret_string.trim_end().to_string()
}

fn type_string(t: &TypeWithGenerics) -> (String, String) {
    match t.type_ {
        Type::Struct(ref field_names, ref field_tss) => {
            let mut insides = if field_tss.len() == 0 {
                "".to_string()
            } else {
                "\n".to_string()
            };
            for field_name in field_names.iter() {
                let ts = field_tss.get(field_name).unwrap();
                insides = format!("{}{}{}: {},\n", insides, tabs(1), field_name, ts_string(ts));
            }
            ("struct".to_string(), insides)
        }
        Type::Enum(ref variants) => {
            let mut insides = if variants.len() == 0 {
                "".to_string()
            } else {
                "\n".to_string()
            };
            for (field_name, tss) in variants.iter() {
                let arg_strings: Vec<String> = tss.iter().map(ts_string).collect();
                let args_if_any = if arg_strings.len() == 0 {
                    "".to_string()
                } else {
                    format!("({})", arg_strings.join(" "))
                };

                insides = format!("{}{}{}{},\n", insides, tabs(1), field_name, args_if_any);
            }
            ("enum".to_string(), insides)
        }
    }
}

fn ts_string(ts: &TypeSig) -> String {
    match ts {
        TypeSig::Reference(s) => s.clone(),
        TypeSig::Function(args, ret) => {
            let arg_strings: Vec<String> = args.iter().map(ts_string).collect();
            format!("Fn({}) -> {}", arg_strings.join(" -> "), ts_string(&*ret))
        }
    }
}

fn expr_string_helper(expr: &Expression, indent_level: u8) -> String {
    format!(
        "{}{}",
        tabs(indent_level),
        match expr {
            Expression::VariableReference(v, _) => v.to_string(),
            Expression::FnCall(func, args) => {
                if are_simple(args) {
                    let arg_strings: Vec<String> =
                        args.iter().map(|x| expr_string_helper(x, 0)).collect();
                    format!(
                        "({} {})",
                        expr_string_helper(&*func, 0).trim_start(),
                        arg_strings.join(" ")
                    )
                } else {
                    let arg_strings: Vec<String> = args
                        .iter()
                        .map(|x| expr_string_helper(x, indent_level + 1))
                        .collect();
                    format!(
                        "({}\n{}\n{})",
                        expr_string_helper(&*func, indent_level + 1).trim_start(),
                        arg_strings.join("\n"),
                        tabs(indent_level)
                    )
                }
            }
            Expression::Lambda(params_with_ts, body, _generics, ret_ts, lazy) => {
                let arg_strings: Vec<String> = params_with_ts
                    .iter()
                    .map(|(name, ts)| format!("{}: {}", name, ts_string(ts)))
                    .collect();
                format!(
                    "({}lambda |{}| -> {}\n{}\n{})",
                    if *lazy { "lazy_" } else { "" },
                    arg_strings.join(", "),
                    ts_string(ret_ts),
                    expr_string_helper(body, indent_level + 1),
                    tabs(indent_level)
                )
            }
            Expression::Struct(struct_name, field_names, args) => {
                let arg_vec: Vec<Expression> = args.values().map(|x| x.clone()).collect();
                if are_simple(&arg_vec) {
                    let arg_strings: Vec<String> = field_names
                        .iter()
                        .map(|k| {
                            let v = args.get(k).unwrap();
                            format!("{}: {}", k, expr_string_helper(v, 0))
                        })
                        .collect();
                    format!("{}{{ {} }}", struct_name, arg_strings.join(", "))
                } else {
                    let arg_strings: Vec<String> = field_names
                        .iter()
                        .map(|k| {
                            let v = args.get(k).unwrap();
                            format!(
                                "{}{}: {},",
                                tabs(indent_level + 1),
                                k,
                                expr_string_helper(v, indent_level + 1).trim_start()
                            )
                        })
                        .collect();
                    format!(
                        "{}{{\n{}\n{}}}",
                        struct_name,
                        arg_strings.join("\n"),
                        tabs(indent_level)
                    )
                }
            }
            Expression::StructFieldAccess(struct_, field_name) => format!(
                "{}.{}",
                expr_string_helper(&*struct_, indent_level + 1).trim_start(),
                field_name
            ),
            Expression::Enum(enum_, variant, bodies) => {
                if are_simple(bodies) {
                    let arg_strings: Vec<String> =
                        bodies.iter().map(|x| expr_string_helper(x, 0)).collect();
                    let args_if_any = if arg_strings.len() == 0 {
                        "".to_string()
                    } else {
                        format!("({})", arg_strings.join(" "))
                    };
                    format!("{}::{}{}", enum_, variant, args_if_any)
                } else {
                    let arg_strings: Vec<String> = bodies
                        .iter()
                        .map(|x| expr_string_helper(x, indent_level + 1))
                        .collect();
                    let args_if_any = if arg_strings.len() == 0 {
                        "".to_string()
                    } else {
                        format!("(\n{}\n{})", arg_strings.join("\n"), tabs(indent_level))
                    };
                    format!("{}::{}{}", enum_, variant, args_if_any)
                }
            }
            Expression::MatchExpr(switcher, arms) => {
                let mut arm_string = "".to_string();
                for (case_, value) in arms.iter() {
                    arm_string = format!(
                        "{}\n{} => {}",
                        arm_string,
                        expr_string_helper(case_, indent_level + 1),
                        expr_string_helper(value, indent_level + 1).trim_start()
                    )
                }
                format!(
                    "(match {}{}\n{})",
                    expr_string_helper(switcher, indent_level + 2).trim_start(),
                    arm_string.trim_end(),
                    tabs(indent_level)
                )
            }
        }
    )
}

fn value_string_helper(value: &Value, indent_level: u8) -> String {
    format!(
        "{}{}",
        tabs(indent_level),
        match value {
            Value::Lambda(params_with_ts, body, _generics, ret_ts, lazy) => {
                let arg_strings: Vec<String> = params_with_ts
                    .iter()
                    .map(|(name, ts)| format!("{}: {}", name, ts_string(ts)))
                    .collect();
                format!(
                    "({}lambda |{}| -> {}\n{}\n{})",
                    if *lazy { "lazy_" } else { "" },
                    arg_strings.join(", "),
                    ts_string(ret_ts),
                    expr_string_helper(&body.clone(), indent_level + 1),
                    tabs(indent_level)
                )
            }
            Value::Struct(struct_name, field_names, args) => {
                let arg_vec: Vec<Value> = args.values().map(|x| x.clone()).collect();
                if are_simple_values(&arg_vec) {
                    let arg_strings: Vec<String> = field_names
                        .iter()
                        .map(|k| {
                            let v = args.get(k).unwrap();
                            format!("{}: {}", k, value_string_helper(v, 0))
                        })
                        .collect();
                    format!("{}{{ {} }}", struct_name, arg_strings.join(", "))
                } else {
                    let arg_strings: Vec<String> = field_names
                        .iter()
                        .map(|k| {
                            let v = args.get(k).unwrap();
                            format!(
                                "{}{}: {},",
                                tabs(indent_level + 1),
                                k,
                                value_string_helper(v, indent_level + 1).trim_start()
                            )
                        })
                        .collect();
                    format!(
                        "{}{{\n{}\n{}}}",
                        struct_name,
                        arg_strings.join("\n"),
                        tabs(indent_level)
                    )
                }
            }
            Value::Enum(enum_, variant, bodies) => {
                if are_simple_values(bodies) {
                    let arg_strings: Vec<String> =
                        bodies.iter().map(|x| value_string_helper(x, 0)).collect();
                    let args_if_any = if arg_strings.len() == 0 {
                        "".to_string()
                    } else {
                        format!("({})", arg_strings.join(" "))
                    };
                    format!("{}::{}{}", enum_, variant, args_if_any)
                } else {
                    let arg_strings: Vec<String> = bodies
                        .iter()
                        .map(|x| value_string_helper(x, indent_level + 1))
                        .collect();
                    let args_if_any = if arg_strings.len() == 0 {
                        "".to_string()
                    } else {
                        format!("(\n{}\n{})", arg_strings.join("\n"), tabs(indent_level))
                    };
                    format!("{}::{}{}", enum_, variant, args_if_any)
                }
            }
        }
    )
}

fn tabs(indent_level: u8) -> String {
    "    ".repeat(indent_level as usize)
}

fn are_simple_values(values: &Vec<Value>) -> bool {
    values
        .iter()
        .fold(true, |res, value| res && is_simple_value(value))
}

fn is_simple_value(value: &Value) -> bool {
    match value {
        Value::Lambda(_, _, _, _, _) => false,
        Value::Struct(_, _, _) => false,
        Value::Enum(_, _, insides) => insides.len() == 0,
    }
}

fn are_simple(exprs: &Vec<Expression>) -> bool {
    exprs.iter().fold(true, |res, expr| res && is_simple(expr))
}

fn is_simple(expr: &Expression) -> bool {
    match expr {
        Expression::VariableReference(_, _) => true,
        Expression::FnCall(_, _) => false,
        Expression::Lambda(_, _, _, _, _) => false,
        Expression::Struct(_, _, _) => false,
        Expression::StructFieldAccess(struct_, _) => is_simple(struct_),
        Expression::Enum(_, _, insides) => insides.len() == 0,
        Expression::MatchExpr(_, _) => false,
    }
}
