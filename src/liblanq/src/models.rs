use std::collections::hash_map::{IntoIter, Iter};
use std::collections::HashMap;

// Shared/Utility/Error Types

#[derive(Debug, Eq, PartialEq)]
pub enum Error {
    TrailingTokensOnExpression(Vec<Token>),
    ExpectedIdentifier(Token),
    ExpectedClose(Token),
    ExpectedVerticalBar(Token),
    ExpectedStartOfExpression(Token),
    ExpectedArrowOrCloseParenInFnTypeSig(Token),
    ExpectedCommaOrPipeInLambda(Token),
    ExpectedCommaOrCloseCurlyInStructInst(Token),
    TypeError,
    TypeAnnotationDoesNotMatchInferredTypeSig(Option<String>, TypeSig, TypeSig),
    VariableDoesNotExist(String),
    UnknownParseableTypeSig(String),
    NodeNotCallable(Expression),
    FieldAccessOfNonStruct,
    FieldDoesNotExist(String),
    FunctionArgumentCountMismatch(i32, i32),
    StructFieldTypeMatchFieldMismatch(String, String, Vec<String>, Vec<String>),
    MatchExpressionWithNoBranches,
    MatchExpressionWithoutMatchingBranch,
    MatchArmMismatch(String, String),
    CannotRebindVariable(String),
    CannotRebindType(String),
    TypeDoesNotExist(String),
    EnumInstantiationOfNonEnum,
    VariantRequestOfNonEnum(String),
    EnumVaiantDoesNotExist(String, String),
    IllegalTopLevelDeclaration(Token),
    ExpectedToken(Token, Token),
    ExpectedTypeDeclaration(Token),
    ExpectedVariableName(Token),
    ExpectedFieldName(Token),
    ExpectedVariantName(Token),
    ExpectedTypeName(Token),
    ExpectedGenericIdentifier(Token),
    ExpectedSpecificIdentifier(String, Token),
    ExpectedCallDirective(Token),
    CannotInstantiateNonStructAsStruct,
    NoLocalBindings,
    UnexpectedEOF,
}

#[derive(Debug, Clone)]
pub struct Bindings<T> {
    inner_map: HashMap<String, T>,
}

impl<T> Bindings<T> {
    pub fn new() -> Bindings<T> {
        return Bindings {
            inner_map: HashMap::new(),
        };
    }

    pub fn from_hash_map(m: HashMap<String, T>) -> Bindings<T> {
        Bindings { inner_map: m }
    }

    pub fn get_ref(&self, s: &String) -> Option<&T> {
        return self.inner_map.get(s);
    }

    pub fn get_ref_mut(&mut self, s: &String) -> Option<&mut T> {
        return self.inner_map.get_mut(s);
    }

    pub fn bind(&mut self, s: String, t: T) -> Result<(), Error> {
        match self.inner_map.get(&s) {
            Some(_) => Err(Error::CannotRebindVariable(s)),
            None => {
                self.inner_map.insert(s, t);
                Ok(())
            }
        }
    }

    pub fn ensure_not_present(&self, s: &String) -> Result<(), Error> {
        match self.inner_map.get(s) {
            Some(_) => Err(Error::CannotRebindVariable(s.clone())),
            None => Ok(()),
        }
    }

    pub fn iter(&self) -> Iter<String, T> {
        self.inner_map.iter()
    }

    pub fn into_iter(self) -> IntoIter<String, T> {
        self.inner_map.into_iter()
    }
}

impl<T: Clone> Bindings<T> {
    pub fn get(&self, s: &String) -> Option<T> {
        return self.inner_map.get(s).map(|x| x.clone());
    }
}

// Tokenization Type(s)

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Token {
    OpenParen,
    CloseParen,
    Comma,
    Period,
    OpenCurly,
    CloseCurly,
    OpenAngleBracket,
    CloseAngleBracket,
    VerticalBar,
    SingleColon,
    DoubleColon,
    Arrow,
    DoubleArrow,
    Identifier(String),
}

// Evaluation and Type Checking Types

#[derive(Debug, Clone)]
pub struct SigAnnotatedBindings {
    pub values_with_ts: Bindings<(Expression, TypeSig)>,
    pub types: Bindings<TypeWithGenerics>,
    // Used to keep track of order of bindings, be them values or types
    pub all_seen: Vec<String>,
}

impl SigAnnotatedBindings {
    pub fn new_empty() -> SigAnnotatedBindings {
        return SigAnnotatedBindings {
            values_with_ts: Bindings::new(),
            types: Bindings::new(),
            all_seen: Vec::new(),
        };
    }

    pub fn into_type_value_bindings(self) -> TypeValueBindings {
        TypeValueBindings {
            values: Bindings {
                inner_map: self
                    .values_with_ts
                    .into_iter()
                    .map(|(k, (v, _))| (k, v))
                    .collect(),
            },
            types: self.types,
        }
    }
}

#[derive(Debug, Clone)]
pub struct TypeValueBindings {
    pub values: Bindings<Expression>,
    pub types: Bindings<TypeWithGenerics>,
}

#[derive(Debug, Clone)]
pub struct TypeWithGenerics {
    pub type_: Type,
    pub generics: Vec<String>,
}

#[derive(Debug, Clone)]
pub enum Type {
    Struct(Vec<String>, HashMap<String, TypeSig>),
    Enum(Vec<(String, Vec<TypeSig>)>),
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TypeSig {
    Reference(String),
    Function(Vec<TypeSig>, Box<TypeSig>),
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Expression {
    // Instantiated variant of the base enumeration type (can hold Expressions inside the variant)
    Enum(String, String, Vec<Expression>),
    // Instantiated struct, with full mapping of each field's value, keyed by the string name of
    // the struct. The extra Vec<String> is to keep track of the order, especially useful when
    // instantiating.
    Struct(String, Vec<String>, HashMap<String, Expression>),
    // Declaration of a lambda, with a vector of the parameters (and their annotated type
    // signatures) and the Expression node to evaluate with arguments bound to the parameters (and the
    // resulting type signature of evaluating the lambda)
    Lambda(
        Vec<(String, TypeSig)>,
        Box<Expression>,
        Vec<String>,
        TypeSig,
        bool,
    ),
    // Reference to another Expression to be accessed by string name matching a binding in scope
    VariableReference(String, Option<TypeSig>),
    // Application of a lambda with arguments to satisfy the parameters
    FnCall(Box<Expression>, Vec<Expression>),
    // A match expression, with one Expression to evaluate and compare against the first of the Expressions in
    // the tuple and when it finds an equal expression it will return the second of that tuple to
    // evaluate
    MatchExpr(Box<Expression>, Vec<(Expression, Expression)>),
    // Access of a struct's field
    StructFieldAccess(Box<Expression>, String),
    // Replacement of a struct's named field with a new value, copying the rest of the fields
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Value {
    // Instantiated variant of the base enumeration type (can hold Values inside the variant)
    Enum(String, String, Vec<Value>),
    // Instantiated struct, with full mapping of each field's value, keyed by the string name of
    // the struct. The extra Vec<String> is to keep track of the order, especially useful when
    // instantiating.
    Struct(String, Vec<String>, HashMap<String, Value>),
    // Declaration of a lambda, with a vector of the parameters (and their annotated type
    // signatures) and the Expression node to evaluate with arguments bound to the parameters (and the
    // resulting type signature of evaluating the lambda)
    Lambda(
        Vec<(String, TypeSig)>,
        Box<Expression>,
        Vec<String>,
        TypeSig,
        bool,
    ),
}

impl Value {
    pub fn into_expr(self) -> Expression {
        match self {
            Value::Enum(enum_name, variant_name, values) => Expression::Enum(
                enum_name,
                variant_name,
                values.into_iter().map(|x| x.into_expr().clone()).collect(),
            ),
            Value::Struct(struct_name, field_names, values) => Expression::Struct(
                struct_name,
                field_names,
                values
                    .into_iter()
                    .map(|(n, x)| (n, x.into_expr()))
                    .collect(),
            ),
            Value::Lambda(params, body, types, ret_ts, lazy) => {
                Expression::Lambda(params, Box::new(*body.clone()), types, ret_ts, lazy)
            }
        }
    }
}
