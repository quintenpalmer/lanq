use models::{Error, Expression, TypeSig};
use std::collections::HashMap;

pub fn hint_lambda_params(expr: Expression) -> Result<Expression, Error> {
    match expr {
        Expression::Lambda(params_with_ts, body, generics, ret_type_sig, lazy) => {
            let new_body = hint_lambda_params_internal(&params_with_ts, *body)?;
            Ok(Expression::Lambda(
                params_with_ts,
                Box::new(new_body),
                generics,
                ret_type_sig,
                lazy,
            ))
        }
        _ => Ok(expr.clone()),
    }
}

fn hint_lambda_params_internal(
    params_with_ts: &Vec<(String, TypeSig)>,
    body: Expression,
) -> Result<Expression, Error> {
    match body {
        Expression::VariableReference(var_name, inferred_type) => {
            for &(ref param_name, ref param_ts) in params_with_ts.iter() {
                if *param_name == *var_name {
                    match &inferred_type {
                        &Some(ref inf_type) => {
                            (if *inf_type != *param_ts {
                                return Err(Error::TypeError);
                            } else {
                                return Ok(Expression::VariableReference(
                                    var_name,
                                    inferred_type.clone(),
                                ));
                            })
                        }
                        &None => {
                            return Ok(Expression::VariableReference(
                                var_name,
                                Some(param_ts.clone()),
                            ))
                        }
                    }
                }
            }
            return Ok(Expression::VariableReference(var_name, inferred_type));
        }
        Expression::Enum(type_name, variant_name, variant_bodies) => {
            let mut new_variant_bodies = Vec::new();
            for variant_body in variant_bodies.into_iter() {
                let new_body = hint_lambda_params_internal(params_with_ts, variant_body)?;
                new_variant_bodies.push(new_body);
            }
            Ok(Expression::Enum(
                type_name,
                variant_name,
                new_variant_bodies,
            ))
        }
        Expression::Struct(struct_name, field_names, fields) => {
            let mut new_field_values = HashMap::new();
            for (field_name, field_value) in fields.into_iter() {
                let new_field_value = hint_lambda_params_internal(params_with_ts, field_value)?;
                new_field_values.insert(field_name, new_field_value);
            }
            Ok(Expression::Struct(
                struct_name,
                field_names,
                new_field_values,
            ))
        }
        Expression::Lambda(params, lambda_body, generic_types, ret_type_sig, lazy) => {
            let new_lambda_body = hint_lambda_params_internal(params_with_ts, *lambda_body)?;
            hint_lambda_params(Expression::Lambda(
                params,
                Box::new(new_lambda_body),
                generic_types,
                ret_type_sig,
                lazy,
            ))
        }
        Expression::FnCall(fn_body, args) => {
            let new_body = hint_lambda_params_internal(params_with_ts, *fn_body)?;
            let mut new_args = Vec::new();
            for arg in args.into_iter() {
                let new_arg = hint_lambda_params_internal(params_with_ts, arg)?;
                new_args.push(new_arg);
            }
            Ok(Expression::FnCall(Box::new(new_body), new_args))
        }
        Expression::MatchExpr(cond_expr, branches) => {
            let new_cond_expr = hint_lambda_params_internal(params_with_ts, *cond_expr)?;
            let mut new_branches = Vec::new();
            for (key, value) in branches.into_iter() {
                let new_key = hint_lambda_params_internal(params_with_ts, key)?;
                let new_value = hint_lambda_params_internal(params_with_ts, value)?;
                new_branches.push((new_key, new_value));
            }
            Ok(Expression::MatchExpr(Box::new(new_cond_expr), new_branches))
        }
        Expression::StructFieldAccess(struct_, field_name) => {
            let new_struct = hint_lambda_params_internal(params_with_ts, *struct_)?;
            Ok(Expression::StructFieldAccess(
                Box::new(new_struct),
                field_name.clone(),
            ))
        }
    }
}
