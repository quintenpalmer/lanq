use std::iter::IntoIterator;

use models::{Bindings, Error, Expression, SigAnnotatedBindings, Type, TypeSig, TypeWithGenerics};
use type_augment;

impl SigAnnotatedBindings {
    pub fn check_expr_bindings(self) -> Result<SigAnnotatedBindings, Error> {
        let mut expr_bindings = SigAnnotatedBindings::new_empty();
        for (k, v) in self.types.iter() {
            v.check_type_with_generics(&self.types)?;
            expr_bindings.types.bind(k.clone(), v.clone())?;
        }
        for (k, (v, t)) in self.values_with_ts.into_iter() {
            v.check_expr(&self.types)?;
            let new_value = type_augment::hint_lambda_params(v)?;
            t.check_type_sig(&self.types)?;
            expr_bindings
                .values_with_ts
                .bind(k.clone(), (new_value, t))?;
        }
        return Ok(expr_bindings);
    }
}

impl TypeWithGenerics {
    fn check_type_with_generics(&self, types: &Bindings<TypeWithGenerics>) -> Result<(), Error> {
        self.type_.check_type(types)?;
        Ok(())
    }
}

impl Type {
    fn check_type(&self, types: &Bindings<TypeWithGenerics>) -> Result<(), Error> {
        match self {
            Type::Enum(variants) => {
                for &(ref _variant_name, ref variant_types) in variants.iter() {
                    for variant_type in variant_types.iter() {
                        variant_type.check_type_sig(types)?;
                    }
                }
                Ok(())
            }
            Type::Struct(_fields, type_sigs) => {
                for (_k, v) in type_sigs.iter() {
                    v.check_type_sig(types)?;
                }
                Ok(())
            }
        }
    }
}

impl TypeSig {
    fn check_type_sig(&self, types: &Bindings<TypeWithGenerics>) -> Result<(), Error> {
        match self {
            TypeSig::Function(args, ret) => {
                for arg in args.into_iter() {
                    arg.check_type_sig(types)?;
                }
                ret.check_type_sig(types)?;
                Ok(())
            }
            TypeSig::Reference(s) => {
                let _ = types.get(&s).ok_or(Error::TypeDoesNotExist(s.clone()))?;
                Ok(())
            }
        }
    }
}

impl Expression {
    pub fn check_expr(&self, types: &Bindings<TypeWithGenerics>) -> Result<(), Error> {
        match self {
            Expression::Struct(struct_name, _field_names, args) => {
                let type_ = types
                    .get(&struct_name)
                    .ok_or(Error::TypeDoesNotExist(struct_name.clone()))?;
                type_.check_type_with_generics(types)?;
                match type_.type_ {
                    Type::Struct(field_names, _) => {
                        if field_names.len() != args.len() {
                            return Err(Error::FunctionArgumentCountMismatch(
                                field_names.len() as i32,
                                args.len() as i32,
                            ));
                        }
                        for field_name in field_names.iter() {
                            let arg = args
                                .get(field_name)
                                .ok_or(Error::FieldDoesNotExist(field_name.to_string()))?;
                            arg.check_expr(types)?;
                        }
                        Ok(())
                    }
                    _ => Err(Error::CannotInstantiateNonStructAsStruct),
                }
            }
            Expression::Enum(enum_, variant, values) => {
                let type_ = types
                    .get(&enum_)
                    .ok_or(Error::TypeDoesNotExist(enum_.clone()))?;
                type_.check_type_with_generics(types)?;
                match type_.type_ {
                    Type::Enum(variants) => {
                        let variant_names: Vec<String> =
                            variants.iter().map(|&(ref x, _)| x.clone()).collect();
                        if !variant_names.contains(&variant) {
                            return Err(Error::EnumVaiantDoesNotExist(
                                enum_.clone(),
                                variant.clone(),
                            ));
                        }
                        let mut expr_values = Vec::new();
                        for value in values.iter() {
                            expr_values.push(value.check_expr(types)?);
                        }
                        Ok(())
                    }
                    _ => Err(Error::CannotInstantiateNonStructAsStruct),
                }
            }
            Expression::Lambda(args, body, _generic_types, ret_parse_type_sig, _lazy) => {
                for (_, y) in args.iter() {
                    y.check_type_sig(types)?;
                }
                let _ret_type_sig = ret_parse_type_sig.check_type_sig(types)?;
                body.check_expr(types)?;
                Ok(())
            }
            Expression::VariableReference(_, _) => Ok(()),
            Expression::FnCall(func, args) => {
                func.check_expr(types)?;
                args.into_iter()
                    .map(|x| x.check_expr(types))
                    .collect::<Result<(), Error>>()?;
                Ok(())
            }
            Expression::MatchExpr(cond_expr, branches) => {
                for &(ref i, ref t) in branches.iter() {
                    i.check_expr(types)?;
                    t.check_expr(types)?;
                }
                cond_expr.check_expr(types)?;
                Ok(())
            }
            Expression::StructFieldAccess(struct_, _field_name) => {
                struct_.check_expr(types)?;
                Ok(())
            }
        }
    }
}
