use lookups;
use models::{Bindings, Error, Expression, SigAnnotatedBindings, TypeSig};
use type_infer;

impl SigAnnotatedBindings {
    pub fn validate_binding_type_sigs(&self) -> Result<(), Error> {
        for (ref name, (ref expr, ref type_sig)) in self.values_with_ts.iter() {
            expr.validate_type_sig(
                Some(name.clone().to_string()),
                &type_sig,
                self,
                &Bindings::new(),
            )?;
        }
        Ok(())
    }
}

impl Expression {
    pub fn validate_type_sig(
        &self,
        name: Option<String>,
        type_sig: &TypeSig,
        bindings: &SigAnnotatedBindings,
        local_bindings: &Bindings<TypeSig>,
    ) -> Result<(), Error> {
        let inferred_type_sig = type_infer::infer_type_signature(self, bindings, local_bindings)?;
        if inferred_type_sig != *type_sig {
            return Err(Error::TypeAnnotationDoesNotMatchInferredTypeSig(
                name,
                type_sig.clone(),
                inferred_type_sig,
            ));
        }
        match *self {
            Expression::Enum(ref enum_name, ref variant_name, ref values) => {
                let expected_type_sigs =
                    lookups::lookup_enum_variant_types(enum_name, variant_name, &bindings.types)?;
                if values.len() != expected_type_sigs.len() {
                    return Err(Error::FunctionArgumentCountMismatch(
                        values.len() as i32,
                        expected_type_sigs.len() as i32,
                    ));
                }
                for (ref value, ref expected_type_sig) in
                    values.iter().zip(expected_type_sigs.iter())
                {
                    value.validate_type_sig(None, expected_type_sig, bindings, local_bindings)?;
                }
                Ok(())
            }
            Expression::Struct(ref struct_name, ref _field_names, ref values) => {
                let field_type_sigs =
                    lookups::lookup_struct_field_types(struct_name, &bindings.types)?;
                for (ref field_name, ref value) in values.iter() {
                    let expected_type_sig = field_type_sigs
                        .get(field_name.as_str())
                        .ok_or(Error::FieldDoesNotExist(field_name.to_string()))?;
                    value.validate_type_sig(None, expected_type_sig, bindings, local_bindings)?;
                }
                Ok(())
            }
            Expression::Lambda(ref params, ref body, _, ref ret_type_sig, _) => {
                let mut new_local_bindings = Bindings::new();
                for &(ref param_name, ref param_type_sig) in params.iter() {
                    new_local_bindings.bind(param_name.clone(), param_type_sig.clone())?;
                }
                body.validate_type_sig(None, ret_type_sig, bindings, &new_local_bindings)?;
                Ok(())
            }
            Expression::VariableReference(_, _) => Ok(()),
            Expression::FnCall(ref body, ref args) => {
                match type_infer::infer_type_signature(body, bindings, local_bindings)? {
                    TypeSig::Function(ref param_type_sigs, _) => {
                        if args.len() != param_type_sigs.len() {
                            return Err(Error::FunctionArgumentCountMismatch(
                                args.len() as i32,
                                param_type_sigs.len() as i32,
                            ));
                        }
                        for (arg, type_sig) in args.iter().zip(param_type_sigs.iter()) {
                            arg.validate_type_sig(None, type_sig, bindings, local_bindings)?;
                        }
                        Ok(())
                    }
                    _ => Err(Error::TypeError),
                }
            }
            Expression::MatchExpr(ref cond_expr, ref branches) => {
                let branch_type_sig =
                    type_infer::infer_type_signature(cond_expr, bindings, local_bindings)?;
                let result_type_sig =
                    type_infer::infer_type_signature(self, bindings, local_bindings)?;
                for &(ref branch_expr, ref then_expr) in branches.iter() {
                    let sub_local_bindings = type_infer::match_expression_new_bindings(
                        branch_expr,
                        &branch_type_sig,
                        bindings,
                    )?;
                    let mut new_local_bindings = local_bindings.clone();
                    for (k, v) in sub_local_bindings.into_iter() {
                        new_local_bindings.bind(k, v)?;
                    }
                    branch_expr.validate_type_sig(
                        None,
                        &branch_type_sig,
                        bindings,
                        &new_local_bindings,
                    )?;
                    then_expr.validate_type_sig(
                        None,
                        &result_type_sig,
                        bindings,
                        &new_local_bindings,
                    )?
                }
                Ok(())
            }
            Expression::StructFieldAccess(_, _) => Ok(()),
        }
    }
}
