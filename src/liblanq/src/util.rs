pub struct Walker<T: Clone> {
    inner_vec: Vec<T>,
    index: usize,
}

impl<T: Clone> Walker<T> {
    pub fn new(inner_vec: Vec<T>) -> Walker<T> {
        return Walker {
            inner_vec: inner_vec,
            index: 0,
        };
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.index >= self.inner_vec.len() {
            return None;
        }
        let val = self.inner_vec.get(self.index);
        self.index += 1;
        return val.map(|x| x.clone());
    }

    pub fn peek(&self) -> Option<T> {
        return self.inner_vec.get(self.index).map(|x| x.clone());
    }

    pub fn is_empty(&self) -> bool {
        match self.inner_vec.get(self.index) {
            Some(_) => false,
            None => true,
        }
    }

    pub fn remaining(mut self) -> Vec<T> {
        return self.inner_vec.split_off(self.index);
    }
}
