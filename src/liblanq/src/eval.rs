use std::collections::HashMap;

use match_eval;
use models::{Bindings, Error, Expression, TypeValueBindings, Value};
use subst;

pub fn evaluate(bindings: TypeValueBindings) -> Result<Value, Error> {
    let main_name = "main".to_string();
    let main = bindings
        .values
        .get(&main_name)
        .ok_or(Error::VariableDoesNotExist(main_name.clone()))?;
    return evaluate_expr(
        &mut main.clone(),
        &bindings,
        &mut Bindings::<Expression>::new(),
    );
}

pub fn eval_expr(expr: Expression, bindings: &TypeValueBindings) -> Result<Value, Error> {
    evaluate_expr(&mut expr.clone(), bindings, &mut Bindings::new())
}

fn evaluate_expr(
    expr: &mut Expression,
    bindings: &TypeValueBindings,
    local_bindings: &mut Bindings<Expression>,
) -> Result<Value, Error> {
    match expr {
        Expression::Enum(enum_name, variant_name, ref mut values) => Ok(Value::Enum(
            enum_name.clone(),
            variant_name.clone(),
            values
                .into_iter()
                .map(|x| evaluate_expr(x, bindings, local_bindings))
                .collect::<Result<Vec<Value>, Error>>()?,
        )),
        Expression::Struct(struct_name, field_names, ref mut values) => {
            let mut field_values: HashMap<String, Value> = HashMap::new();
            for (s, field_val) in values.into_iter() {
                let evaled_field_val = evaluate_expr(field_val, bindings, local_bindings)?;
                field_values.insert(s.clone(), evaled_field_val);
            }
            Ok(Value::Struct(
                struct_name.clone(),
                field_names.clone(),
                values
                    .into_iter()
                    .map(|(name, val)| {
                        Ok((name.clone(), evaluate_expr(val, bindings, local_bindings)?))
                    })
                    .collect::<Result<HashMap<String, Value>, Error>>()?,
            ))
        }
        Expression::Lambda(params, body, types, ret_ts, lazy) => Ok(Value::Lambda(
            params.clone(),
            Box::new(*body.clone()),
            types.clone(),
            ret_ts.clone(),
            lazy.clone(),
        )),
        Expression::VariableReference(name, _) => {
            let mut var_expr = match local_bindings.get_ref(&name) {
                Some(v) => v.clone(),
                None => bindings
                    .values
                    .get_ref(&name)
                    .ok_or(Error::VariableDoesNotExist(name.clone()))?
                    .clone(),
            };
            evaluate_expr(&mut var_expr, bindings, local_bindings)
        }
        Expression::FnCall(func_expr, ref mut args) => {
            let mut evaled_func_expr = evaluate_expr(&mut *func_expr, bindings, local_bindings)?;
            match evaled_func_expr {
                Value::Lambda(ref var_names, ref mut node, _, _, lazy) => {
                    if var_names.len() != args.len() {
                        return Err(Error::FunctionArgumentCountMismatch(
                            var_names.len() as i32,
                            args.len() as i32,
                        ));
                    }

                    let mut lambda_args_lookup =
                        Bindings::from_hash_map(
                            var_names
                                .into_iter()
                                .map(|(x, _)| x.clone())
                                .zip(args.iter_mut().map(|x| {
                                    subst::resolve_local_bindings(x, local_bindings).clone()
                                }))
                                .collect::<HashMap<String, Expression>>(),
                        );

                    if !lazy {
                        lambda_args_lookup = Bindings::from_hash_map(
                            lambda_args_lookup
                                .into_iter()
                                .map(|(k, mut v)| {
                                    Ok((
                                        k,
                                        evaluate_expr(&mut v, bindings, local_bindings)?
                                            .into_expr(),
                                    ))
                                })
                                .collect::<Result<HashMap<String, Expression>, Error>>()?,
                        );
                    }

                    let mut new_node = subst::resolve_local_bindings(node, &mut lambda_args_lookup);
                    evaluate_expr(&mut new_node, bindings, &mut Bindings::new())
                }
                _ => Err(Error::NodeNotCallable(*func_expr.clone())),
            }
        }
        Expression::MatchExpr(cond_expr, ref mut branches) => {
            let evalled_cond_expr = evaluate_expr(&mut *cond_expr, bindings, local_bindings)?;
            for (if_expr, then_expr) in branches.into_iter() {
                match match_eval::resolve_match_expression(&if_expr, &evalled_cond_expr)? {
                    (true, sub_local_bindings) => {
                        let mut new_local_bindings = local_bindings.clone();
                        for (k, v) in sub_local_bindings.into_iter() {
                            new_local_bindings.bind(k, v)?;
                        }
                        return evaluate_expr(then_expr, bindings, &mut new_local_bindings);
                    }
                    (false, _) => (),
                };
            }
            return Err(Error::MatchExpressionWithoutMatchingBranch);
        }
        Expression::StructFieldAccess(ref mut struct_, ref field_name) => {
            match evaluate_expr(struct_, bindings, local_bindings)? {
                Value::Struct(_, _, fields) => match fields.get(field_name) {
                    Some(v) => Ok(v.clone()),
                    None => Err(Error::FieldDoesNotExist(field_name.clone())),
                },
                _ => Err(Error::FieldAccessOfNonStruct),
            }
        }
    }
}
