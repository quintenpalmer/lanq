#[macro_export]
macro_rules! gen_expr {
    ($string:expr, $expected_ts:expr, $bindings:expr, $generic_types:expr) => {{
        let tokens = $crate::tokenize($string);
        let expr = match $crate::parse_expression_from_tokens(tokens, $generic_types) {
            Ok(p) => match p.check_expr_and_type_sig($expected_ts, $bindings) {
                Ok(e) => e,
                Err(e) => panic!("could not check expression: {:?}", e),
            },
            Err(e) => panic!("could not parse expression: {:?}", e),
        };
        expr
    }};
}

#[macro_export]
macro_rules! gen_bindings {
    ($string:expr) => {{
        let tokens = $crate::tokenize($string);
        let bindings = match $crate::parse_all_bindings(tokens) {
            Ok(unchecked_bindings) => match unchecked_bindings.check_exprs_and_type_sigs() {
                Ok(b) => b,
                Err(e) => panic!("could not check bindings: {:?}", e),
            },
            Err(e) => panic!("could not parse bindings: {:?}", e),
        };
        bindings
    }};
}
