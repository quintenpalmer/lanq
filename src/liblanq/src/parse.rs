use std::collections::HashMap;

use models::{Error, Expression, SigAnnotatedBindings, Token, Type, TypeSig, TypeWithGenerics};
use util;

pub fn parse_all_bindings(tokens: Vec<Token>) -> Result<SigAnnotatedBindings, Error> {
    let mut bindings = SigAnnotatedBindings::new_empty();
    let mut token_walker = util::Walker::<Token>::new(tokens);
    parse_all_top_level_declarations(&mut token_walker, &mut bindings)?;
    if !token_walker.is_empty() {
        return Err(Error::TrailingTokensOnExpression(token_walker.remaining()));
    }
    return Ok(bindings);
}

pub fn parse_expression_from_tokens(
    tokens: Vec<Token>,
    generic_types: &Vec<String>,
) -> Result<Expression, Error> {
    let mut token_walker = util::Walker::<Token>::new(tokens);
    let expr = parse_expression(&mut token_walker, generic_types)?;
    if !token_walker.is_empty() {
        return Err(Error::TrailingTokensOnExpression(token_walker.remaining()));
    }
    return Ok(expr);
}

fn parse_all_top_level_declarations(
    tokens: &mut util::Walker<Token>,
    bindings: &mut SigAnnotatedBindings,
) -> Result<(), Error> {
    parse_top_level_declaration(tokens, bindings)?;
    if tokens.is_empty() {
        return Ok(());
    }
    return parse_all_top_level_declarations(tokens, bindings);
}

fn parse_top_level_declaration(
    tokens: &mut util::Walker<Token>,
    bindings: &mut SigAnnotatedBindings,
) -> Result<(), Error> {
    let (top_level_str, token) =
        parse_identifier_with_token(tokens, Error::IllegalTopLevelDeclaration)?;
    match top_level_str.as_str() {
        "let" => {
            let var_name = parse_identifier(tokens, Error::ExpectedVariableName)?;
            let generic_types = parse_generic_type_definition(tokens)?;
            parse_expect_token(tokens, Token::SingleColon)?;
            let type_sig = parse_type_sig(tokens)?;
            parse_expect_string(tokens, "=".to_string())?;
            let expr = parse_expression(tokens, &generic_types)?;
            bindings.types.ensure_not_present(&var_name)?;
            bindings.all_seen.push(var_name.clone());
            bindings.values_with_ts.bind(var_name, (expr, type_sig))?;
            Ok(())
        }
        "fn" => {
            let var_name = parse_identifier(tokens, Error::ExpectedVariableName)?;
            let generic_types = parse_generic_type_definition(tokens)?;
            parse_expect_token(tokens, Token::OpenParen)?;
            let param_names = parse_idents_until_close(tokens, Token::CloseParen, Vec::new())?;
            parse_expect_token(tokens, Token::CloseParen)?;
            parse_expect_token(tokens, Token::Arrow)?;
            let ret_type_sig = parse_type_sig(tokens)?;

            let mut param_type_sigs = Vec::new();
            for &(_, ref param_type_sig) in param_names.iter() {
                param_type_sigs.push(param_type_sig.clone());
            }

            let body = parse_expression(tokens, &generic_types)?;
            let expr = Expression::Lambda(
                param_names,
                Box::new(body),
                generic_types,
                ret_type_sig.clone(),
                false,
            );

            bindings.types.ensure_not_present(&var_name)?;
            bindings.all_seen.push(var_name.clone());
            bindings.values_with_ts.bind(
                var_name,
                (
                    expr,
                    TypeSig::Function(param_type_sigs, Box::new(ret_type_sig)),
                ),
            )?;
            Ok(())
        }
        "lazy_fn" => {
            let var_name = parse_identifier(tokens, Error::ExpectedVariableName)?;
            let generic_types = parse_generic_type_definition(tokens)?;
            parse_expect_token(tokens, Token::OpenParen)?;
            let param_names = parse_idents_until_close(tokens, Token::CloseParen, Vec::new())?;
            parse_expect_token(tokens, Token::CloseParen)?;
            parse_expect_token(tokens, Token::Arrow)?;
            let ret_type_sig = parse_type_sig(tokens)?;

            let mut param_type_sigs = Vec::new();
            for &(_, ref param_type_sig) in param_names.iter() {
                param_type_sigs.push(param_type_sig.clone());
            }

            let body = parse_expression(tokens, &generic_types)?;
            let expr = Expression::Lambda(
                param_names,
                Box::new(body),
                generic_types,
                ret_type_sig.clone(),
                true,
            );

            bindings.types.ensure_not_present(&var_name)?;
            bindings.all_seen.push(var_name.clone());
            bindings.values_with_ts.bind(
                var_name,
                (
                    expr,
                    TypeSig::Function(param_type_sigs, Box::new(ret_type_sig)),
                ),
            )?;
            Ok(())
        }
        "type" => {
            let type_name = parse_identifier(tokens, Error::ExpectedTypeName)?;
            let generic_types = parse_generic_type_definition(tokens)?;
            let (type_declaration, td_token) =
                parse_identifier_with_token(tokens, Error::ExpectedTypeDeclaration)?;
            let new_type = match type_declaration.as_str() {
                "struct" => parse_struct(tokens),
                "enum" => parse_enum(tokens),
                _ => Err(Error::ExpectedTypeDeclaration(td_token)),
            }?;
            let type_with_generics = TypeWithGenerics {
                type_: new_type,
                generics: generic_types,
            };
            bindings.values_with_ts.ensure_not_present(&type_name)?;
            bindings.all_seen.push(type_name.clone());
            bindings.types.bind(type_name, type_with_generics)?;
            Ok(())
        }
        _ => Err(Error::IllegalTopLevelDeclaration(token.clone())),
    }
}

fn parse_struct(tokens: &mut util::Walker<Token>) -> Result<Type, Error> {
    parse_expect_token(tokens, Token::OpenCurly)?;
    let (fields, field_types) = parse_fields_until_close_curly(tokens, Vec::new(), HashMap::new())?;
    parse_expect_token(tokens, Token::CloseCurly)?;
    return Ok(Type::Struct(fields, field_types));
}

fn parse_enum(tokens: &mut util::Walker<Token>) -> Result<Type, Error> {
    parse_expect_token(tokens, Token::OpenCurly)?;
    let variants = parse_enum_variants_until_close_curly(tokens, Vec::new())?;
    parse_expect_token(tokens, Token::CloseCurly)?;
    return Ok(Type::Enum(variants));
}

fn parse_generic_type_definition(tokens: &mut util::Walker<Token>) -> Result<Vec<String>, Error> {
    let maybe_open_angle = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    Ok(match maybe_open_angle {
        Token::OpenAngleBracket => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            let s = parse_identifier(tokens, Error::ExpectedGenericIdentifier)?;
            let types = parse_generic_type_definitions_until_close_angle(tokens, vec![s])?;
            parse_expect_token(tokens, Token::CloseAngleBracket)?;
            types
        }
        _ => Vec::new(),
    })
}

fn parse_generic_type_definitions_until_close_angle(
    tokens: &mut util::Walker<Token>,
    mut types: Vec<String>,
) -> Result<Vec<String>, Error> {
    let maybe_close_angle = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_close_angle {
        Token::CloseAngleBracket => return Ok(types),
        Token::Comma => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            let s = parse_identifier(tokens, Error::ExpectedGenericIdentifier)?;
            types.push(s.clone());
        }
        _ => return Err(Error::ExpectedGenericIdentifier(maybe_close_angle.clone())),
    };
    return parse_generic_type_definitions_until_close_angle(tokens, types);
}

fn parse_expect_string(tokens: &mut util::Walker<Token>, expected: String) -> Result<(), Error> {
    let (string, tok) = parse_identifier_with_token(tokens, |token| {
        Error::ExpectedSpecificIdentifier(expected.clone(), token)
    })?;
    if string != expected {
        return Err(Error::ExpectedSpecificIdentifier(expected, tok));
    }
    return Ok(());
}

fn parse_identifier_with_token<F>(
    tokens: &mut util::Walker<Token>,
    err_func: F,
) -> Result<(String, Token), Error>
where
    F: FnOnce(Token) -> Error,
{
    let maybe_ident_token = tokens.pop().ok_or(Error::UnexpectedEOF)?;
    match maybe_ident_token.clone() {
        Token::Identifier(s) => Ok((s.clone(), maybe_ident_token.clone())),
        _ => Err(err_func(maybe_ident_token.clone())),
    }
}

fn parse_identifier<F>(tokens: &mut util::Walker<Token>, err_func: F) -> Result<String, Error>
where
    F: FnOnce(Token) -> Error,
{
    let (s, _) = parse_identifier_with_token(tokens, err_func)?;
    return Ok(s);
}

fn parse_expression(
    tokens: &mut util::Walker<Token>,
    generic_types: &Vec<String>,
) -> Result<Expression, Error> {
    let start_token = tokens.pop().ok_or(Error::UnexpectedEOF)?;
    let base_expression = match start_token {
        Token::OpenParen => {
            let call_switch_token = tokens.peek().ok_or(Error::UnexpectedEOF)?;
            match call_switch_token {
                Token::Identifier(s) => match s.as_str() {
                    "lambda" => {
                        tokens.pop().ok_or(Error::UnexpectedEOF)?;
                        parse_expect_token(tokens, Token::VerticalBar)?;
                        let var_names =
                            parse_idents_until_close(tokens, Token::VerticalBar, Vec::new())?;
                        parse_expect_token(tokens, Token::VerticalBar)?;
                        parse_expect_token(tokens, Token::Arrow)?;
                        let ret_type_sig = parse_type_sig(tokens)?;
                        let expr = parse_expression(tokens, generic_types)?;
                        parse_expect_token(tokens, Token::CloseParen)?;
                        return Ok(Expression::Lambda(
                            var_names,
                            Box::new(expr),
                            generic_types.clone(),
                            ret_type_sig,
                            false,
                        ));
                    }
                    "lazy_lambda" => {
                        tokens.pop().ok_or(Error::UnexpectedEOF)?;
                        parse_expect_token(tokens, Token::VerticalBar)?;
                        let var_names =
                            parse_idents_until_close(tokens, Token::VerticalBar, Vec::new())?;
                        parse_expect_token(tokens, Token::VerticalBar)?;
                        parse_expect_token(tokens, Token::Arrow)?;
                        let ret_type_sig = parse_type_sig(tokens)?;
                        let expr = parse_expression(tokens, generic_types)?;
                        parse_expect_token(tokens, Token::CloseParen)?;
                        return Ok(Expression::Lambda(
                            var_names,
                            Box::new(expr),
                            generic_types.clone(),
                            ret_type_sig,
                            true,
                        ));
                    }
                    "match" => {
                        tokens.pop().ok_or(Error::UnexpectedEOF)?;
                        let check_expr = parse_expression(tokens, generic_types)?;
                        let branches = parse_match_branches(tokens, Vec::new(), generic_types)?;
                        parse_expect_token(tokens, Token::CloseParen)?;
                        return Ok(Expression::MatchExpr(Box::new(check_expr), branches));
                    }
                    _ => (),
                },
                _ => (),
            };
            let function_expr = parse_expression(tokens, generic_types)?;
            let args = parse_expressions_until_close(tokens, Vec::new(), generic_types)?;
            parse_expect_token(tokens, Token::CloseParen)?;
            return Ok(Expression::FnCall(Box::new(function_expr), args));
        }
        Token::Identifier(i) => match tokens.peek() {
            Some(Token::DoubleColon) => {
                let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
                let enum_variant = parse_ident(tokens)?;
                let variant_bodies = match tokens.peek() {
                    Some(Token::OpenParen) => {
                        let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
                        let vbs = parse_expressions_until_close(tokens, Vec::new(), generic_types)?;
                        parse_expect_token(tokens, Token::CloseParen)?;
                        vbs
                    }
                    Some(_) => Vec::new(),
                    None => Vec::new(),
                };
                Ok(Expression::Enum(i, enum_variant, variant_bodies))
            }
            Some(Token::OpenCurly) => {
                tokens.pop().ok_or(Error::UnexpectedEOF)?;
                let (field_names, field_values) = parse_field_insts_until_close_curly(
                    tokens,
                    Vec::new(),
                    HashMap::new(),
                    generic_types,
                )?;
                parse_expect_token(tokens, Token::CloseCurly)?;
                return Ok(Expression::Struct(i, field_names, field_values));
            }
            Some(_) => Ok(Expression::VariableReference(i, None)),
            None => Ok(Expression::VariableReference(i, None)),
        },
        _ => Err(Error::ExpectedStartOfExpression(start_token.clone())),
    }?;
    parse_post_expression(base_expression, tokens)
}

fn parse_post_expression(
    base_expression: Expression,
    tokens: &mut util::Walker<Token>,
) -> Result<Expression, Error> {
    let peek_token = match tokens.peek() {
        Some(v) => v,
        None => return Ok(base_expression),
    };
    match peek_token {
        Token::Period => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            let field_name = parse_ident(tokens)?;
            parse_post_expression(
                Expression::StructFieldAccess(Box::new(base_expression), field_name),
                tokens,
            )
        }
        _ => Ok(base_expression),
    }
}

fn parse_ident(tokens: &mut util::Walker<Token>) -> Result<String, Error> {
    let token = tokens.pop().ok_or(Error::UnexpectedEOF)?;
    match token {
        Token::Identifier(s) => Ok(s),
        _ => Err(Error::ExpectedIdentifier(token.clone())),
    }
}

fn parse_idents_until_close(
    tokens: &mut util::Walker<Token>,
    close: Token,
    mut var_names: Vec<(String, TypeSig)>,
) -> Result<Vec<(String, TypeSig)>, Error> {
    let maybe_ident_token = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_ident_token.clone() {
        Token::Identifier(s) => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            parse_expect_token(tokens, Token::SingleColon)?;
            let type_sig = parse_type_sig(tokens)?;
            var_names.push((s.clone(), type_sig));
            match tokens.peek().ok_or(Error::UnexpectedEOF)? {
                Token::Comma => {
                    let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
                    parse_idents_until_close(tokens, close, var_names)
                }
                x => {
                    if x == close {
                        Ok(var_names)
                    } else {
                        Err(Error::ExpectedCommaOrPipeInLambda(x.clone()))
                    }
                }
            }
        }
        s => {
            if s == close {
                Ok(var_names)
            } else {
                Err(Error::ExpectedVariableName(maybe_ident_token.clone()))
            }
        }
    }
}

fn parse_enum_variants_until_close_curly(
    tokens: &mut util::Walker<Token>,
    mut variant_names: Vec<(String, Vec<TypeSig>)>,
) -> Result<Vec<(String, Vec<TypeSig>)>, Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_close {
        Token::CloseCurly => return Ok(variant_names),
        Token::Identifier(s) => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            let maybe_open = tokens.peek().ok_or(Error::UnexpectedEOF)?;
            let type_sigs = match maybe_open {
                Token::OpenParen => {
                    let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
                    let ts = parse_type_sigs_until_close(tokens, Vec::new())?;
                    parse_expect_token(tokens, Token::CloseParen)?;
                    ts
                }
                _ => Vec::new(),
            };
            parse_expect_token(tokens, Token::Comma)?;
            variant_names.push((s.clone(), type_sigs))
        }
        _ => return Err(Error::ExpectedVariantName(maybe_close.clone())),
    };
    return parse_enum_variants_until_close_curly(tokens, variant_names);
}

fn parse_type_sigs_until_close(
    tokens: &mut util::Walker<Token>,
    mut type_sigs: Vec<TypeSig>,
) -> Result<Vec<TypeSig>, Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    if maybe_close == Token::CloseParen {
        return Ok(type_sigs);
    }
    let type_sig = parse_type_sig(tokens)?;
    type_sigs.push(type_sig);
    return parse_type_sigs_until_close(tokens, type_sigs);
}

fn parse_fields_until_close_curly(
    tokens: &mut util::Walker<Token>,
    mut field_names: Vec<String>,
    mut field_types: HashMap<String, TypeSig>,
) -> Result<(Vec<String>, HashMap<String, TypeSig>), Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_close {
        Token::CloseCurly => return Ok((field_names, field_types)),
        Token::Identifier(s) => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            parse_expect_token(tokens, Token::SingleColon)?;
            let type_sig = parse_type_sig(tokens)?;
            parse_expect_token(tokens, Token::Comma)?;
            field_names.push(s.clone());
            field_types.insert(s.clone(), type_sig);
        }
        _ => return Err(Error::ExpectedFieldName(maybe_close.clone())),
    };
    return parse_fields_until_close_curly(tokens, field_names, field_types);
}

fn parse_field_insts_until_close_curly(
    tokens: &mut util::Walker<Token>,
    mut field_names: Vec<String>,
    mut field_types: HashMap<String, Expression>,
    generic_types: &Vec<String>,
) -> Result<(Vec<String>, HashMap<String, Expression>), Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_close {
        Token::Identifier(ref s) => {
            let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
            parse_expect_token(tokens, Token::SingleColon)?;
            let expr = parse_expression(tokens, generic_types)?;
            field_names.push(s.clone());
            field_types.insert(s.clone(), expr);
            match tokens.peek().ok_or(Error::UnexpectedEOF)? {
                Token::Comma => {
                    let _ = tokens.pop().ok_or(Error::UnexpectedEOF)?;
                    parse_field_insts_until_close_curly(
                        tokens,
                        field_names,
                        field_types,
                        generic_types,
                    )
                }
                Token::CloseCurly => Ok((field_names, field_types)),
                _ => Err(Error::ExpectedCommaOrCloseCurlyInStructInst(
                    maybe_close.clone(),
                )),
            }
        }
        Token::CloseCurly => Ok((field_names, field_types)),
        _ => Err(Error::ExpectedFieldName(maybe_close.clone())),
    }
}

fn parse_expressions_until_close(
    tokens: &mut util::Walker<Token>,
    mut arguments: Vec<Expression>,
    generic_types: &Vec<String>,
) -> Result<Vec<Expression>, Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    if maybe_close == Token::CloseParen {
        return Ok(arguments);
    }
    let argument = parse_expression(tokens, generic_types)?;
    arguments.push(argument);
    return parse_expressions_until_close(tokens, arguments, generic_types);
}

fn parse_match_branches(
    tokens: &mut util::Walker<Token>,
    mut branches: Vec<(Expression, Expression)>,
    generic_types: &Vec<String>,
) -> Result<Vec<(Expression, Expression)>, Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    if maybe_close == Token::CloseParen {
        return Ok(branches);
    }
    let branch_if_expr = parse_expression(tokens, generic_types)?;
    parse_expect_token(tokens, Token::DoubleArrow)?;
    let branch_then_expr = parse_expression(tokens, generic_types)?;
    branches.push((branch_if_expr, branch_then_expr));
    return parse_match_branches(tokens, branches, generic_types);
}

fn parse_type_sig(tokens: &mut util::Walker<Token>) -> Result<TypeSig, Error> {
    let s = parse_ident(tokens)?;
    match s.as_ref() {
        "Fn" => {
            parse_expect_token(tokens, Token::OpenParen)?;
            parse_func_type_sig(tokens, Vec::new())
        }
        _ => Ok(TypeSig::Reference(s)),
    }
}

fn parse_func_type_sig(
    tokens: &mut util::Walker<Token>,
    mut arg_types: Vec<TypeSig>,
) -> Result<TypeSig, Error> {
    let maybe_close = tokens.peek().ok_or(Error::UnexpectedEOF)?;
    match maybe_close {
        Token::CloseParen => consume_end_func_type_sig(tokens, arg_types),
        _ => {
            let arg_type = parse_type_sig(tokens)?;
            arg_types.push(arg_type);
            let maybe_arrow = tokens.peek().ok_or(Error::UnexpectedEOF)?;
            match maybe_arrow {
                Token::Arrow => {
                    tokens.pop().ok_or(Error::UnexpectedEOF)?;
                    parse_func_type_sig(tokens, arg_types)
                }
                Token::CloseParen => consume_end_func_type_sig(tokens, arg_types),
                _ => Err(Error::ExpectedArrowOrCloseParenInFnTypeSig(
                    maybe_arrow.clone(),
                )),
            }
        }
    }
}

fn consume_end_func_type_sig(
    tokens: &mut util::Walker<Token>,
    arg_types: Vec<TypeSig>,
) -> Result<TypeSig, Error> {
    parse_expect_token(tokens, Token::CloseParen)?;
    parse_expect_token(tokens, Token::Arrow)?;
    let ret = parse_type_sig(tokens)?;
    return Ok(TypeSig::Function(arg_types, Box::new(ret)));
}

fn parse_expect_token(tokens: &mut util::Walker<Token>, token: Token) -> Result<(), Error> {
    let maybe_token = tokens.pop().ok_or(Error::UnexpectedEOF)?;
    if maybe_token == token {
        return Ok(());
    } else {
        return Err(Error::ExpectedToken(token.clone(), maybe_token.clone()));
    }
}
