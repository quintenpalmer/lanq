use models::Token;
use util;

pub fn tokenize(source: String) -> Vec<Token> {
    let mut char_walker =
        util::Walker::<char>::new(source.into_bytes().into_iter().map(|x| x as char).collect());
    return tokenize_helper(&mut char_walker, Vec::new());
}

fn tokenize_helper(char_walker: &mut util::Walker<char>, mut tokens: Vec<Token>) -> Vec<Token> {
    if char_walker.is_empty() {
        return tokens;
    }
    let buffer = String::new();
    let o_tokens = parse_token_or_tokens(char_walker, buffer);
    match o_tokens {
        Some(mut ts) => tokens.append(&mut ts),
        None => (),
    };
    return tokenize_helper(char_walker, tokens);
}

fn parse_token_or_tokens(
    char_walker: &mut util::Walker<char>,
    mut buffer: String,
) -> Option<Vec<Token>> {
    if char_walker.is_empty() {
        if buffer.is_empty() {
            return None;
        } else {
            return match maybe_token_from_buffer(buffer) {
                Some(b) => Some(vec![b]),
                None => None,
            };
        }
    }
    let o_c = char_walker.pop();
    match o_c {
        Some(c) => match c {
            '{' => return Some(build_tokens_with_buffer(buffer, Token::OpenCurly)),
            '}' => return Some(build_tokens_with_buffer(buffer, Token::CloseCurly)),
            '.' => return Some(build_tokens_with_buffer(buffer, Token::Period)),
            ',' => return Some(build_tokens_with_buffer(buffer, Token::Comma)),
            '(' => return Some(build_tokens_with_buffer(buffer, Token::OpenParen)),
            ')' => return Some(build_tokens_with_buffer(buffer, Token::CloseParen)),
            '<' => return Some(build_tokens_with_buffer(buffer, Token::OpenAngleBracket)),
            '>' => return Some(build_tokens_with_buffer(buffer, Token::CloseAngleBracket)),
            '|' => return Some(build_tokens_with_buffer(buffer, Token::VerticalBar)),
            ' ' | '\t' | '\n' => {
                return match maybe_token_from_buffer(buffer) {
                    Some(b) => Some(vec![b]),
                    None => None,
                };
            }
            ':' => {
                match char_walker.peek() {
                    Some(':') => {
                        let _ = char_walker.pop();
                        return Some(build_tokens_with_buffer(buffer, Token::DoubleColon));
                    }
                    Some(_) => {
                        return Some(build_tokens_with_buffer(buffer, Token::SingleColon));
                    }
                    None => {
                        return None;
                    }
                };
            }
            '-' => {
                match char_walker.peek() {
                    Some('>') => {
                        let _ = char_walker.pop();
                        return Some(build_tokens_with_buffer(buffer, Token::Arrow));
                    }
                    Some(_) => {
                        return Some(build_tokens_with_buffer(
                            buffer,
                            Token::Identifier("-".to_string()),
                        ));
                    }
                    None => {
                        return None;
                    }
                };
            }
            '=' => {
                match char_walker.peek() {
                    Some('>') => {
                        let _ = char_walker.pop();
                        return Some(build_tokens_with_buffer(buffer, Token::DoubleArrow));
                    }
                    Some('=') => {
                        let _ = char_walker.pop();
                        return Some(build_tokens_with_buffer(
                            buffer,
                            Token::Identifier("==".to_string()),
                        ));
                    }
                    Some(_) => {
                        return Some(build_tokens_with_buffer(
                            buffer,
                            Token::Identifier("=".to_string()),
                        ));
                    }
                    None => {
                        return None;
                    }
                };
            }
            _ => buffer.push(c),
        },
        None => return None,
    };
    return parse_token_or_tokens(char_walker, buffer);
}

fn build_tokens_with_buffer(buffer: String, token: Token) -> Vec<Token> {
    match maybe_token_from_buffer(buffer) {
        Some(b) => vec![b, token],
        None => vec![token],
    }
}

fn maybe_token_from_buffer(buffer: String) -> Option<Token> {
    if buffer.is_empty() {
        return None;
    }
    return Some(Token::Identifier(buffer));
}
