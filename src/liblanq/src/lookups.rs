use std::collections::HashMap;

use models::{Bindings, Error, Type, TypeSig, TypeWithGenerics};

pub fn lookup_struct_field_types(
    struct_name: &String,
    types: &Bindings<TypeWithGenerics>,
) -> Result<HashMap<String, TypeSig>, Error> {
    let type_ = types
        .get(struct_name)
        .ok_or(Error::TypeDoesNotExist(struct_name.clone()))?;
    match type_.type_ {
        Type::Struct(_, field_type_sigs) => Ok(field_type_sigs),
        _ => Err(Error::FieldAccessOfNonStruct),
    }
}

pub fn lookup_enum_variant_types(
    enum_name: &String,
    variant_name: &String,
    types: &Bindings<TypeWithGenerics>,
) -> Result<Vec<TypeSig>, Error> {
    let type_ = types
        .get(&enum_name)
        .ok_or(Error::TypeDoesNotExist(enum_name.clone()))?;
    match type_.type_ {
        Type::Enum(variants) => {
            for (actual_variant_name, exprs) in variants.into_iter() {
                if actual_variant_name == *variant_name {
                    return Ok(exprs);
                }
            }
            Err(Error::EnumVaiantDoesNotExist(
                enum_name.clone(),
                variant_name.clone(),
            ))
        }
        _ => Err(Error::VariantRequestOfNonEnum(enum_name.clone())),
    }
}
