use models::{Bindings, Expression};

pub fn resolve_local_bindings<'a>(
    expr: &'a mut Expression,
    local_bindings: &'a mut Bindings<Expression>,
) -> &'a mut Expression {
    match expr {
        Expression::VariableReference(ref name, _) => match local_bindings.get_ref_mut(&name) {
            Some(new_expr) => {
                *expr = new_expr.clone();
                expr
            }
            None => expr,
        },
        Expression::Enum(_, _, ref mut inner_exprs) => {
            *inner_exprs = inner_exprs
                .iter_mut()
                .map(|x| resolve_local_bindings(x, local_bindings).clone())
                .collect();
            expr
        }
        Expression::Struct(_, _, ref mut fields) => {
            *fields = fields
                .iter_mut()
                .map(|(name, field)| {
                    (
                        name.to_string(),
                        resolve_local_bindings(field, local_bindings).clone(),
                    )
                })
                .collect();
            expr
        }
        Expression::Lambda(_, ref mut inner_body, _, _, _) => {
            *inner_body = Box::new(resolve_local_bindings(inner_body, local_bindings).clone());
            expr
        }
        Expression::FnCall(ref mut func, ref mut more_args) => {
            *func = Box::new(resolve_local_bindings(func, local_bindings).clone());
            *more_args = more_args
                .iter_mut()
                .map(|more_arg| resolve_local_bindings(more_arg, local_bindings).clone())
                .collect();
            expr
        }
        Expression::MatchExpr(ref mut match_expr, ref mut arms) => {
            *match_expr = Box::new(resolve_local_bindings(match_expr, local_bindings).clone());
            *arms = arms
                .iter_mut()
                .map(|(k, v)| (k.clone(), resolve_local_bindings(v, local_bindings).clone()))
                .collect();
            expr
        }
        Expression::StructFieldAccess(ref mut struct_expr, _) => {
            *struct_expr = Box::new(resolve_local_bindings(struct_expr, local_bindings).clone());
            expr
        }
    }
}
