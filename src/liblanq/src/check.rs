use models::{Bindings, Error, Expression, SigAnnotatedBindings, TypeSig};
use type_infer;

impl SigAnnotatedBindings {
    pub fn check_exprs_and_type_sigs(self) -> Result<SigAnnotatedBindings, Error> {
        let type_expr_bindings = self.check_expr_bindings()?;
        type_expr_bindings.validate_binding_type_sigs()?;
        return Ok(type_expr_bindings);
    }
}

impl Expression {
    pub fn check_expr_and_type_sig(
        self,
        expected_ts: &TypeSig,
        bindings: &SigAnnotatedBindings,
    ) -> Result<Expression, Error> {
        self.check_expr(&bindings.types)?;
        self.validate_type_sig(None, expected_ts, bindings, &Bindings::new())?;
        return Ok(self);
    }
}

impl Expression {
    pub fn check_expr_and_type_sig_infer_type_sig(
        self,
        bindings: &SigAnnotatedBindings,
    ) -> Result<Expression, Error> {
        self.check_expr(&bindings.types)?;
        let expected_ts = type_infer::infer_type_signature(&self, bindings, &Bindings::new())?;
        self.validate_type_sig(None, &expected_ts, bindings, &Bindings::new())?;
        return Ok(self);
    }
}
