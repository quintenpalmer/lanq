#[macro_use]
extern crate liblanq;

use std::env;
use std::io::{self, Read, Write};

fn main() {
    match run_main() {
        Ok(_) => (),
        Err(e) => {
            match e {
                Error::InvalidInvocation => println!("Must provide one of the following commands:\n\tlanqi run <source-file>\n\tlanqi debug-run <source-file>\n\tlanqi interactive <optional-source-file>"),
                _ => println!("error: {:?}", e),
            };
            std::process::exit(1)
        }
    };
}

fn run_main() -> Result<(), Error> {
    let args = env::args().collect::<Vec<String>>();
    if args.len() < 2 {
        return Err(Error::InvalidInvocation);
    }
    let o_source_file = if args.len() == 3 {
        Some(args.get(2).unwrap().as_str())
    } else {
        None
    };
    match args.get(1).unwrap().as_str() {
        "run" => {
            let source_file = match o_source_file {
                Some(v) => Ok(v),
                None => Err(Error::InvalidInvocation),
            }?;
            run_file(source_file, false)
        }
        "debug-run" => {
            let source_file = match o_source_file {
                Some(v) => Ok(v),
                None => Err(Error::InvalidInvocation),
            }?;
            run_file(source_file, true)
        }
        "interactive" => start_interactive_repl(o_source_file),
        _ => {
            return Err(Error::InvalidInvocation);
        }
    }
}

fn run_file(source_file: &str, debug: bool) -> Result<(), Error> {
    let mut file = std::fs::File::open(source_file)?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    if debug {
        println!("input string: {}", input);
    }
    let tokens = liblanq::tokenize(input);
    if debug {
        println!("tokens: [");
        for token in tokens.iter() {
            println!("\t{:?},", token);
        }
        println!("]");
    }
    let parse_bindings = liblanq::parse_all_bindings(tokens)?;
    if debug {
        println!("parse bindings are:",);
        println!("```");
        println!(
            "{}",
            liblanq::write_pretty_sig_annotated_bindings(&parse_bindings)
        );
        println!("```");
    }

    let bindings = match parse_bindings.check_exprs_and_type_sigs() {
        Ok(b) => {
            if debug {
                println!("passed type checker");
            }
            Ok(b)
        }
        Err(e) => {
            if debug {
                println!("failed the type checker");
            }
            Err(e)
        }
    }?;

    let res = liblanq::evaluate(bindings.into_type_value_bindings())?;
    if debug {
        println!("result is: {}", liblanq::value_string(res));
    } else {
        println!("{}", liblanq::value_string(res));
    }
    return Ok(());
}

fn start_interactive_repl(o_bindings_file: Option<&str>) -> Result<(), Error> {
    let o_bindings: Result<liblanq::SigAnnotatedBindings, Error> = match o_bindings_file {
        Some(binding_file) => {
            let mut file = std::fs::File::open(binding_file)?;
            let mut input = String::new();
            file.read_to_string(&mut input)?;

            let tokens = liblanq::tokenize(input);
            let parse_bindings = liblanq::parse_all_bindings(tokens)?;
            let bindings = parse_bindings.check_exprs_and_type_sigs()?;
            Ok(bindings)
        }
        None => Ok(gen_bindings!("
                        type Bool enum {
                            True,
                            False,
                        }"
        .to_string())),
    };
    let bindings = o_bindings?;
    let value_bindings = bindings.clone().into_type_value_bindings();
    loop {
        io::stdout().write(b"=> ")?;
        io::stdout().flush()?;

        let mut input = String::new();
        let num_bytes = io::stdin().read_line(&mut input)?;
        if num_bytes == 0 {
            println!("<EOF>");
            println!("exiting");
            return Ok(());
        }
        let input = input.trim();
        if input == "quit" || input == "exit" {
            println!("exiting");
            return Ok(());
        }

        let tokens = liblanq::tokenize(input.to_string());
        let expr = match liblanq::parse_expression_from_tokens(tokens, &vec![]) {
            Ok(v) => v,
            Err(e) => {
                println!("err: {:?}", e);
                continue;
            }
        };
        let expr = match expr.check_expr_and_type_sig_infer_type_sig(&bindings) {
            Ok(v) => v,
            Err(e) => {
                println!("err: {:?}", e);
                continue;
            }
        };

        let res = match liblanq::eval_expr(expr, &value_bindings) {
            Ok(v) => v,
            Err(e) => {
                println!("err: {:?}", e);
                continue;
            }
        };
        println!("{}", liblanq::value_string(res));
    }
}

#[derive(Debug)]
enum Error {
    InvalidInvocation,
    LanqError(liblanq::Error),
    IO(std::io::Error),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IO(error)
    }
}

impl From<liblanq::Error> for Error {
    fn from(error: liblanq::Error) -> Self {
        Error::LanqError(error)
    }
}
