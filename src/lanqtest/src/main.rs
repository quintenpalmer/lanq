extern crate liblanq;

use std::env;
use std::ffi::OsStr;
use std::fs;
use std::io::{self, Read};

fn main() {
    match run_main() {
        Ok(_) => println!("OK! (all tests pass)"),
        Err(e) => {
            match e {
                Error::CmdLineArg(message) => println!("{}", message),
                _ => println!("ERROR: {:?}", e),
            };
            std::process::exit(1)
        }
    };
}

fn run_main() -> Result<(), Error> {
    let args = env::args().collect::<Vec<String>>();
    if args.len() != 2 {
        return Err(Error::CmdLineArg(
            "Error, incorrect invocation.\n\nMust include the directory where all of the test `.lq` and `.lq.ex` files are.\ne.g.\n\tcargo run --manifest-path src/lanqtest/Cargo.toml src/lanqtest/tests/)"
                .to_string(),
        ));
    }
    test_all_in_dir(args.get(1).unwrap())
}

fn test_all_in_dir(test_dir: &str) -> Result<(), Error> {
    let paths = fs::read_dir(test_dir)?;

    for path in paths {
        let path = path?;
        let filename = path.path();
        if filename.extension().unwrap_or(OsStr::new("")) == "lq" {
            let mut file = fs::File::open(filename.clone())?;
            let mut input = String::new();
            file.read_to_string(&mut input)?;

            let tokens = liblanq::tokenize(input);
            let parse_bindings = liblanq::parse_all_bindings(tokens)?;
            let bindings = parse_bindings.check_exprs_and_type_sigs()?;

            let res = liblanq::evaluate(bindings.into_type_value_bindings())?;
            let mut expected_file = fs::File::open(filename.with_extension("lq.ex"))?;
            let mut expected = String::new();
            expected_file.read_to_string(&mut expected)?;

            let expected_tokens = liblanq::tokenize(expected);
            let expected_expr =
                liblanq::parse_expression_from_tokens(expected_tokens, &Vec::new())?;

            if expected_expr != res.clone().into_expr() {
                return Err(Error::NotEqual(
                    liblanq::expr_string(expected_expr),
                    liblanq::value_string(res),
                ));
            } else {
                println!("ok  ({})", path.file_name().to_string_lossy());
            }
        }
    }
    Ok(())
}

#[derive(Debug)]
enum Error {
    NotEqual(String, String),
    CmdLineArg(String),
    LanqError(liblanq::Error),
    IO(io::Error),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IO(error)
    }
}

impl From<liblanq::Error> for Error {
    fn from(error: liblanq::Error) -> Self {
        Error::LanqError(error)
    }
}
