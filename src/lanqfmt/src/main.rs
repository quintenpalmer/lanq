extern crate liblanq;

use std::io::{self, Read};

fn main() {
    match run_main() {
        Ok(_) => (),
        Err(e) => {
            println!("error: {:?}", e);
            std::process::exit(1)
        }
    };
}

fn run_main() -> Result<(), Error> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;
    println!("{}", liblanq::write_pretty_file(input.trim().to_string())?);
    Ok(())
}

#[derive(Debug)]
enum Error {
    LanqError(liblanq::Error),
    IO(std::io::Error),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::IO(error)
    }
}

impl From<liblanq::Error> for Error {
    fn from(error: liblanq::Error) -> Self {
        Error::LanqError(error)
    }
}
