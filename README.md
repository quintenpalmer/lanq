Lanq Programming Language
=========================

This is a Lisp-flavor toy language intended to be reasonably general purpose.

It is written in and heavily inspired by [Rust](https://www.rust-lang.org/).

How to run this project
-----------------------

From the project root, you can run the Lanq interpreter, as shown here:

```shell
 $ cargo run --manifest-path src/lanqi/Cargo.toml interactive lib/prelude_like.lq
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/lanqi interactive lib/prelude_like.lq`
=> (and Bool::True Bool::False)
Bool::False
=> (or Bool::True Bool::False)
Bool::True
=> <EOF>
exiting
```

Example Lanq Source Code
------------------------

### `Bool`

`Bool` type to represent `True` or `False`:

```
type Bool enum {
    False,
    True,
}
```


### `if`

`if` expression for `Bool` type:

```
fn if(b: Bool, then_: Bool, else_: Bool) -> Bool
    (match b
        Bool::True => then_
        Bool::False => else_
    )
```

### `BoolList`

`BoolList` type as a recursively defined linked list for bools

```
type BoolList enum {
    Cons(Bool BoolList),
    Empty,
}
```

### `bool_map`

`bool_map` function that will call given function on each int in a list,
and produce a list with each resulting value:

```
fn bool_map(xs: BoolList, map_fn: Fn(Bool) -> Bool) -> BoolList
    (match xs
        BoolList::Cons(x rest) => BoolList::Cons(
            (map_fn x)
            (bool_map rest map_fn)
        )
        BoolList::Empty => BoolList::Empty
    )
```

### An example file

```
type Bool enum {
    False,
    True,
}

fn not(b: Bool) -> Bool
    (match b
        Bool::False => Bool::True
        Bool::True => Bool::False
    )

fn if(b: Bool, then_: Bool, else_: Bool) -> Bool
    (match b
        Bool::True => then_
        Bool::False => else_
    )

type BoolList enum {
    Cons(Bool BoolList),
    Empty,
}

fn bool_map(xs: BoolList, map_fn: Fn(Bool) -> Bool) -> BoolList
    (match xs
        BoolList::Cons(x rest) => BoolList::Cons(
            (map_fn x)
            (bool_map rest map_fn)
        )
        BoolList::Empty => BoolList::Empty
    )

let main: BoolList =
    (bool_map
        BoolList::Cons(
            Bool::True
            BoolList::Cons(Bool::False BoolList::Empty)
        )
        (lambda |x: Bool| -> Bool
            (not x)
        )
    )
```

Anatomy of the project
----------------------

### liblanq

This is the core of the language; it has all of the data structures,
parsing, checking, evaluation, etc. The Expression and Value types defined in
[models.rs](src/liblanq/src/models.rs) speak well to how the language is
interpreted.

This is a library and cannot be run, but [lib.rs](src/liblanq/src/lib.rs) does
show the surface area of the crate.

### lanqtest

This is a hand-rolled testing framework that takes test scenarios of just
Lanq code and an expected value, and makes sure that each expression of
Lanq code does evaluate the expected value.

This crate can be run with:

```shell
 $ cargo run --manifest-path src/lanqtest/Cargo.toml src/lanqtest/tests/
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/lanqtest src/lanqtest/tests/`
ok  (int.lq)
ok  (closure.lq)
ok  (list.lq)
ok  (add.lq)
ok  (enum_eg.lq)
ok  (simple.lq)
ok  (simple_match.lq)
ok  (eg.lq)
OK! (all tests pass)
```

### lanqi

This is the source for the Lanq interpreter binary (lanqi standing for
"Lanq interpreter"). It can run a provided file and print the output of that
file's `main` function and can also provide an interactive REPL.

Example `lanqi run` invocation (example `lanqi interactive` invocation at top
of README):

```shell
 $ echo "type Bool enum {
    False,
    True,
}

let main: Bool =
    Bool::True
" > simple.lq
~/f/g/lanq * (add-README) (a723971) quinten@quarch 0 13:45:31
 $ cargo run --manifest-path src/lanqi/Cargo.toml run simple.lq
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/lanqi run simple.lq`
Bool::True
```

### lanqfmt

This is the source for the Lanq formatting binary tool. It takes Lanq source
code as input and outputs the same source code formatted accordingly to my
current best guidance on what well-formatted Lanq code should look like.

Example usage:

```shell
 $  echo "type Bool enum { False, True, } let main: Bool = Bool::True " | cargo run --manifest-path src/lanqfmt/Cargo.toml 
   Compiling lanqfmt v0.1.0 (/home/quinten/fun/games/lanq/src/lanqfmt)
    Finished dev [unoptimized + debuginfo] target(s) in 0.35s
     Running `target/debug/lanqfmt`
type Bool enum {
    False,
    True,
}

let main: Bool =
    Bool::True
```

### exgample

A portmanteau of sorts of "example" and "game"; this code is a small binary
that leverages some of the macros defined in `liblanq` to interface between Rust
code and Lanq code.

Example invocation (this output isn't useful, but if you compare to the source,
you can see what this is doing):

```shell
 $ cargo run --manifest-path src/exgample/Cargo.toml
    Finished dev [unoptimized + debuginfo] target(s) in 0.01s
     Running `target/debug/exgample`
character: Character { name: "Idrigoth", health: Incr(Incr(Incr(Incr(Incr(Zero))))), strength: Incr(Incr(Zero)), agility: Incr(Incr(Incr(Incr(Zero)))), intelligence: Incr(Incr(Incr(Zero))), single_weapon_attack_bonus_lambda: Lambda([("character", Reference("Character"))], StructFieldAccess(VariableReference("character", None), "agility"), [], Reference("RInt"), false), double_weapon_attack_bonus_lambda: Lambda([("character", Reference("Character"))], StructFieldAccess(VariableReference("character", None), "strength"), [], Reference("RInt"), false) }
single: Ok(4)
single: Ok(2)
```

See the [main.rs](src/exgample/src/main.rs) for how this leverages the macros
to interface between Rust and Lanq code.

### lib/prelude_like.lq

This is Lanq source code that is meant to be the start of a prelude for the language.
I am trying hard to keep much from bleeding into the language as it is defined in liblanq,
in an effort to allow potential different users to only define what they think is useful.

TODOs
-----

* Add support for generics, probably with parametric polymorphism
* Promote prelude_like.lq to an actual prelude that is easier to hook into
  * Also add tests for the functions in this new prelude and/or the existing prelude_like.lq
* Add support for modules so that developers could share code without just copy-pasting
* Better testing facilities
* Build FFI functionality with easy to hook-in facilities
  * Transform the old Int/PrimitiveOperator into something more flexible for people's different use cases

Known Bugs
==========

* Current lambda-argument-substitution evaluation can subtitute arguments into  variables of other lambda bodies
  * Plan is to instead have local bindings actually be a collection of bindings so that we can look up values for the scope we are in
* Enums instantiated with values inside are indistinguishable from enums without values followed by a function call, see the code block
  * We want to apply the provided `lambda` as the second argument to `apply`, but instead the language thinks it is `Bit::Zero(lambda |`
  * We may just have an expression `(enuminst Bool Zero)` to avoid this.

```
type Bit enum {
    Zero,
    One,
}

fn apply(b: Bit, func: Fn(Bit) -> Bit) -> Bit
    (func b)

let main: Bit =
    (apply Bit::Zero (lambda |x: Bit| -> Bit x))
```
